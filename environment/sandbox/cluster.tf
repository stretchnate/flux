resource "google_container_cluster" "vpc_native_cluster" {
    name                     = var.cluster_name
    location                 = var.region
    remove_default_node_pool = true
    initial_node_count       = 1

    network    = "default"
    subnetwork = "default"

    enable_legacy_abac = true
    vertical_pod_autoscaling {
      enabled = true
    }

    min_master_version = "1.18.14-gke.1600"
}

# add a node pool
resource "google_container_node_pool" "primary_nodes" {
  name       = "${var.cluster_name}-node-pool"
  location   = var.region
  cluster    = google_container_cluster.vpc_native_cluster.name
  node_count = 1

  autoscaling {
    max_node_count = 10
    min_node_count = 1
  }

  node_config {
    preemptible  = false
    machine_type = "e2-standard-2"

    tags = [ "ifs-sandbox-cluster" ]
    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/compute",
    ]
  }
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.vpc_native_cluster.name
  description = "GKE Cluster Name"
}

output "cluster_master_ip" {
  value = google_container_cluster.vpc_native_cluster.endpoint
  description = "Cluster Master IP"
}