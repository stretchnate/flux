# variable "target_path" {
#     type = string
#     description = "Relative path to the Git repository root where Flux manifests are committed."
#     default = "clusters/alpha/flux-system"
# }

# variable "sync_target_path" {
#     type = string
#     description = "Relative path to the Git repository root where the sync manifests are committed."
#     default = "clusters/alpha/reconcile"
# }

# variable "clone_url" {
#     type = string
#     default = "ssh://git@gitlab.com/stretchnate/flux"
# }

# variable "clone_branch" {
#     type = string
#     default = "master"
# }

# variable "gitlab_token" {
#     type = string
# }

# variable "gitlab_api" {
#     type = string
#     default = "https://gitlab.com/stretchnate/api/v4/"
# }

# variable "gitlab_deploy_key_prefix" {
#     type = string
#     default = "flux-ssh"
# }

# provider "gitlab" {
#     token    = var.gitlab_token
#     # base_url = var.gitlab_api # not needed for gitlab.com
# }

# locals {
#    known_hosts = var.ssh_known_hosts
# }

# resource "tls_private_key" "main" {
#     algorithm = "ECDSA"
#     ecdsa_curve = "P521"
# }

# data "flux_install" "main" {
#     target_path    = var.target_path
#     network_policy = false
#     version        = "v0.9.1"
# }

# data "flux_sync" "main" {
#     target_path = var.sync_target_path
#     url         = var.clone_url
#     branch      = var.clone_branch
# }

# data "kubectl_file_documents" "apply" {
#     content = data.flux_install.main.content
# }

# # generate and apply yamls containing GitRepository and Kustomization that configures flux to sync with a repository
# data "kubectl_file_documents" "sync" {
#     content = data.flux_sync.main.content
# }

# locals {
#     apply = [ for v in data.kubectl_file_documents.apply.documents : {
#         data: yamldecode(v)
#         content: v
#     }]
#     sync = [ for v in data.kubectl_file_documents.sync.documents : {
#         data: yamldecode(v)
#         content: v
#     }]
# }

# # resource "kubectl_manifest" "apply" {
# #     for_each = { for v in local.apply : lower(join("/", compact([v.data.apiVersion, v.data.kind, lookup(v.data.metadata, "namespace", ""), v.data.metadata.name]))) => v.content}
# #     depends_on = [ kubernetes_namespace.flux_system ]
# #     yaml_body = each.value
# # }

# # resource "kubectl_manifest" "sync" {
# #   for_each   = { for v in local.sync : lower(join("/", compact([v.data.apiVersion, v.data.kind, lookup(v.data.metadata, "namespace", ""), v.data.metadata.name]))) => v.content }
# #   depends_on = [kubernetes_namespace.flux_system]
# #   yaml_body  = each.value
# # }

# resource "kubernetes_secret" "main" {
#     # depends_on = [ kubectl_manifest.apply ]

#     metadata {
#       name      = data.flux_sync.main.name
#       namespace = data.flux_sync.main.namespace
#     }

#     data = {
#         identity       = tls_private_key.main.private_key_pem
#         "identity.pub" = tls_private_key.main.public_key_pem
#         known_hosts    = local.known_hosts
#     }
# }

# # Gitlab
# data "gitlab_project" "flux" {
#     id = 25039105
# }

# resource "gitlab_deploy_key" "main" {
#     project  = data.gitlab_project.flux.id
#     title    = "${var.gitlab_deploy_key_prefix}-${var.app_environment}-${var.cloud_provider}"
#     key      = tls_private_key.main.public_key_openssh
#     can_push = true #defaults to false
# }