# Cloud
variable "cloud_provider" {
    type = string
    description = "the cloud the cluster is hosted on (gcp/aws)"
    default = "gcp"
}

variable "project" { # remove default
    type        = string
    description = "project id"
}

variable "region" {
    description = "region"
    default     = "us-central1"
}

variable "zone" {
    description = "zone"
    default     = "us-central1-c"
}

variable "gcp_credentials" {
    description = "gcp credentials"
}

variable "cluster_name" { # remove default
    type = string
    description = "name of cluster"
}

# Apps
variable "apps_namespace" { #remove Default
    type = string
    description = "namespace for apps"
    default = "sandbox-apps"
}

variable "app_environment" { # remove default
    type = string
    description = "environment for the application (test, alpha, prod)"
    default = "sandbox"
}

# SSH
variable "ssh_known_hosts" {
    type = string
    description = "public key for git repo"
}
