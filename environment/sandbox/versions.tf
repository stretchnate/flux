terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~> 3.5.0"
    }

    google = {
      source = "hashicorp/google"
      version = "~> 3.5.0"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      # version = "~> 1.13"
      version = "~> 2.0"
    }

    helm = {
      source = "hashicorp/helm"
      # version = "~> 1.3"
      version = "~> 2.0"
    }

    flux = { # https://registry.terraform.io/providers/fluxcd/flux/latest
      source = "fluxcd/flux"
      version = "~> 0.1.1"
    }

    kubectl = { # https://registry.terraform.io/providers/gavinbunney/kubectl/latest
      source = "gavinbunney/kubectl"
      version = "~> 1.10.0"
    }

    tls = {
      source = "hashicorp/tls"
      version = "~> 3.1.0"
    }
  }

  required_version = ">= 0.13"
}