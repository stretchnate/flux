data "google_client_config" "provider" {}

provider "helm" {
    kubernetes {
        load_config_file = false

        host = "https://${google_container_cluster.vpc_native_cluster.endpoint}"
        token = data.google_client_config.provider.access_token
        cluster_ca_certificate = base64decode(
            google_container_cluster.vpc_native_cluster.master_auth[0].cluster_ca_certificate
        )
    }
}

provider "kubernetes" {
    # load_config_file = false

    host = "https://${google_container_cluster.vpc_native_cluster.endpoint}"
    token = data.google_client_config.provider.access_token
    cluster_ca_certificate = base64decode(
        google_container_cluster.vpc_native_cluster.master_auth[0].cluster_ca_certificate
    )
}