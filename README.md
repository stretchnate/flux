# flux

This project is set up to test issues I've seen in Flux V2 on GKE. The issues I've noticed are:

1. Cannot install flux manifests (for the flux components) via the flux terraform provider, see flux2.tf (I've commented out the blocks that install and sync flux). i've opened an [issue](https://github.com/fluxcd/terraform-provider-flux/issues/121) with the terraform provider for this one.
2. After Installing Kong via HelmRelease, deleting or commenting out that same HelmRelease does not remove Kong in spite of Prune being enabled on flux.
3. HelmRelease for Kong doesn't seem to create the kong namespace on install.

Terraform Version = v0.14.9
Helm Provider Version = 2.0.3
Kubernetes Provider Version = 2.0.3
Kubectl Provider Version = 1.10.0
Flux Provider Version = 0.1.1

## Cluster Set up
First you'll need to export some TF_VAR_ variables
TF_VAR_gcp_credentials - this is a service account token with Project editor access
TF_VAR_ssh_known_hosts - contents of an ssh known_hosts file with gitlab.com

-  It's probably best to fork this repo
-  Create an access token in Gitlab and export it to $GITLABDOTCOM_TOKEN
-  $ `cd ./environment/sandbox && terraform init`
-  $ `terraform apply -var cluster_name=<your cluster name> -var project=<your project id>`
-  copy kubectl command to connect to your new cluster (should look similar to this `gcloud container clusters get-credentials <your cluster name> --region us-central1 --project <your project id>`)
-  uncomment namespace.tf and rerun last command
-  uncomment fluxv2.tf and run `terraform apply -var cluster_name=<your cluster name> -var project=<your project id> -var gitlab_token=$GITLABDOTCOM_TOKEN` (note the additional var this time)

## Replicating Issue 1
in fluxv2.tf at line 83 you will see a declaration for applying the flux compenents, it should still be commented out, uncomment it and re-run the last command from above. you should see errors similar to the following
```
Error: flux-system/helm-controller failed to create kubernetes rest client for update of resource: Get "http://localhost/api?timeout=32s": dial tcp [::1]:80: connect: connection refused

  on fluxv2.tf line 95, in resource "kubectl_manifest" "apply":
  95: resource "kubectl_manifest" "apply" {
```

## Replicating Issue 2
You can skip Replicating Issue 1 as it has no bearing on this one however you will need to apply the flux compenents and sync as well as the kong_helmrepo.yaml file. You will want to modify gotk-sync.yaml to use the repo you have forked to.   Assuming you are still in ./environment/sandbox
-  $ `kubectl apply -f flux-system/gotk-components.yaml`
-  $ `kubectl apply -f flux-system/gotk-sync.yaml`
-  $ `cd ../../clusters/sandbox`
-  $ `kubectl apply -f kong_helmrepo.yaml`

Watch for Kong to be installed on the cluster (it should take about 2 minutes).  
After kong is installed comment out, commit and push ./clusters/sandbox/reconcile/kong.yaml then watch kong to be uninstalled, it doesn't happen.

## Replicating Issue 3
remove the kong namespace by commenting it out in ./environment/sandbox/namespace.tf and run terraform apply (or create the cluster without the namespace).
follow instructions to Replicate issue 2.